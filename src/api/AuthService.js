import Api from './Api'

export default {
  async signin (username, password) {
    try {
      const response = await Api().post('auth/signin', {
        username: username,
        password: password
      })
      const { user, token } = response.data
      return { user, token }
    } catch (err) {
      console.error(err.response)
      const { response: { data: { error } } } = err
      throw new Error(error)
    }
  },
  async verify (token) {
    try {
      const response = await Api().post('auth/verify', {
        token: token
      })
      const { data } = response
      return data
    } catch (err) {
      console.error(err.response)
      const { response: { data: { error } } } = err
      throw new Error(error)
    }
  }
}
