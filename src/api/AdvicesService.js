import Api from './Api'

export default {
  async index () {
    try {
      const response = await Api().get('advices')
      const advices = response.data.sort((a, b) => (a.date_to > b.date_to))
      return advices
    } catch (err) {
      console.error(err.response)
      const { response: { data: { error } } } = err
      throw new Error(error)
    }
  },
  async get (id) {
    try {
      const response = await Api().get(`advices/${id}`)
      const advice = response.data
      return advice
    } catch (err) {
      console.error(err.response)
      const { response: { data: { error } } } = err
      throw new Error(error)
    }
  },
  async create (advice, token) {
    if (!token) {
      throw new Error('Пользователь не авторизован')
    }
    try {
      const response = await Api().post('advices', advice, {
        headers: { 'x-access-token': token }
      })
      const newAdvice = response.data
      return newAdvice
    } catch (err) {
      console.error(err.response)
      const { response: { data: { error } } } = err
      throw new Error(error)
    }
  },
  async update (advice, token) {
    if (!token) {
      throw new Error('Пользователь не авторизован')
    }
    try {
      const response = await Api().put(`advices/${advice.id}`, advice, {
        headers: { 'x-access-token': token }
      })
      const updatedAdvice = response.data
      return updatedAdvice
    } catch (err) {
      console.error(err.response)
      const { response: { data: { error } } } = err
      throw new Error(error)
    }
  },
  async delete (adviceId, token) {
    if (!token) {
      throw new Error('Пользователь не авторизован')
    }
    try {
      await Api().delete(`advices/${adviceId}`, {
        headers: { 'x-access-token': token }
      })
      return adviceId
    } catch (err) {
      console.error(err.response)
      const { response: { data: { error } } } = err
      throw new Error(error)
    }
  }
}
