import Vue from 'vue'
import Buefy from 'buefy'
import App from './App.vue'

import AuthService from './api/AuthService'

import 'buefy/lib/buefy.css'
Vue.use(Buefy)

Vue.config.productionTip = false

const state = {
  user: null,
  token: ''
}

const app = new Vue({
  render: h => h(App),
  data: {
    state
  },
  async created () {
    const auth = localStorage.getItem('auth')
    if (auth) {
      const { token } = JSON.parse(auth)
      if (token) {
        const data = await AuthService.verify(token)
        this.state.user = data.user
        this.state.token = token
      }
    }
  },
  methods: {
    showError (error) {
      this.$snackbar.open({
        message: error,
        type: 'is-danger',
        position: 'is-top'
      })
    },
    isSignedIn () {
      return !!this.state.user
    },
    storeUserToken (user, token) {
      this.state.user = user
      this.state.token = token
      localStorage.setItem('auth', JSON.stringify({token}))
    },
    clearUserToken () {
      this.state.user = null
      this.state.token = ''
      localStorage.removeItem('auth')
    },
    getUser () {
      return this.state.user
    },
    getToken () {
      return this.state.token
    }
  }
}).$mount('#app')

export default app
